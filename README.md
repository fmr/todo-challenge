# Coding Challenge

Answers to questions

* [tech-test.md](tech-test.md) | [tech-test.pdf](tech-test.pdf)

Single-page HTML:

* [after.html](after.html) - completed challenge tasks
* [after-hooks.html](after-hooks.html) - completed challenge tasks, refactored using [React hooks](https://reactjs.org/docs/hooks-intro.html)



## Development

### Running the app in development mode

Run the app in the development mode.

```bash
yarn start
```

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Run tests

Launch the test runner in the interactive watch mode.

```bash
yarn test
```

![test-results](tests.png)