import React, { memo, useEffect, useState } from "react";
import "./App.css";

export const showButton = (markAsComplete, items = []) =>
  !items
    .filter((item) => item.selected)
    .every((item) => item.complete === markAsComplete);

const Item = memo((props) => {
  const {
    item: { title, complete, selected },
    onToggle,
    onSelect,
  } = props;

  return (
    <div className={`item ${complete ? "complete" : ""}`}>
      <label>
        <input
          type="checkbox"
          onChange={onSelect}
          checked={selected ? "checked" : ""}
        />
        <strong>{title}</strong>
      </label>
      <button onClick={onToggle}>Toggle complete</button>
    </div>
  );
});

const createItems = (num) =>
  new Array(num).fill(null).map((_, i) => ({
    title: `To-do: ${i}`,
    complete: false,
    selected: false,
  }));

const App = memo(() => {
  const [items, setItems] = useState(createItems(35));
  const [showMenu, setShowMenu] = useState(false);

  const bulkComplete = (complete = true) => {
    const updatedItems = items.map((item) =>
      item.selected ? { ...item, complete, selected: false } : item
    );
    setItems(updatedItems);
  };

  const toggleItem = (index) => {
    const updatedItems = items.map((item, i) =>
      index === i ? { ...item, complete: !item.complete } : item
    );
    setItems(updatedItems);
  };

  const selectItem = (index) => {
    const updatedItems = items.map((item, i) =>
      index === i ? { ...item, selected: !item.selected } : item
    );
    setItems(updatedItems);
  };

  useEffect(() => {
    const updatedShowMenu = Boolean(items.find((item) => item.selected));
    setShowMenu(updatedShowMenu);
  }, [items, setShowMenu]);

  const showMarkAsComplete = showButton(true, items);
  const showMarkAsIncomplete = showButton(false, items);

  return (
    <div className="app">
      <header>
        <div className="logo">Sleeping Duck</div>
        <div className="nav">
          <button>I don't do anything</button>
          <button>I also don't do anything</button>
          <button>user</button>
        </div>
      </header>

      <div className="items">
        {items.map((item, i) => (
          <Item
            key={i}
            onSelect={() => selectItem(i)}
            onToggle={() => toggleItem(i)}
            item={item}
          />
        ))}
      </div>

      {showMenu && (
        <div className="actions-menu" data-testid="bulkActions">
          {showMarkAsComplete && (
            <button onClick={() => bulkComplete(true)}>Mark as complete</button>
          )}
          {showMarkAsIncomplete && (
            <button onClick={() => bulkComplete(false)}>
              Mark as incomplete
            </button>
          )}
        </div>
      )}
    </div>
  );
});

export default App;
