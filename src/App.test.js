import React from "react";
import { fireEvent, render } from "@testing-library/react";
import App, { showButton } from "./App";

describe("Challenge goals", () => {
  const queryAllCompletedCheckboxes = (container) =>
    container.querySelectorAll(".item.complete input[type=checkbox]");
  const queryAllIncompleteCheckboxes = (container) =>
    container.querySelectorAll(".item:not(.complete) input[type=checkbox]");

  it('should have a button in called "user"', () => {
    const { getByText } = render(<App />);
    expect(getByText(/user/)).toBeInTheDocument();
  });

  it("should have the same number of checkboxes as todos", () => {
    const { container } = render(<App />);
    const numberOfTodos = 35;
    const checkboxes = container.querySelectorAll("input[type=checkbox]");
    expect(checkboxes).toHaveLength(numberOfTodos);
  });

  it("should show a menu when a checkbox is clicked", () => {
    const { queryByTestId, container } = render(<App />);
    expect(queryByTestId("bulkActions")).not.toBeInTheDocument();
    const firstCheckbox = container.querySelectorAll("input[type=checkbox]")[0];
    fireEvent.click(firstCheckbox);
    expect(queryByTestId("bulkActions")).toBeInTheDocument();
  });

  it("should show 'mark as complete' button when incomplete task is selected", () => {
    const { queryByText, container } = render(<App />);
    const firstIncompleteCheckbox = queryAllIncompleteCheckboxes(container)[0];
    fireEvent.click(firstIncompleteCheckbox);
    expect(queryByText(/Mark as complete/)).toBeInTheDocument();
    expect(queryByText(/Mark as incomplete/)).not.toBeInTheDocument();
  });

  it("should show 'mark as incomplete' button when completed task is selected", () => {
    const { queryByText, queryAllByText, container } = render(<App />);
    const firstToggleButton = queryAllByText(/Toggle complete/)[0];
    fireEvent.click(firstToggleButton);

    const firstCompleteCheckbox = queryAllCompletedCheckboxes(container)[0];
    fireEvent.click(firstCompleteCheckbox);
    expect(queryByText(/Mark as incomplete/)).toBeInTheDocument();
    expect(queryByText(/Mark as complete/)).not.toBeInTheDocument();
  });

  it("should show both bulk actions when a mix of complete and incomplete tasks are selected", () => {
    const { queryByText, queryAllByText, container } = render(<App />);
    const firstToggleButton = queryAllByText(/Toggle complete/)[0];
    fireEvent.click(firstToggleButton);
    const firstCompleteCheckbox = queryAllCompletedCheckboxes(container)[0];
    const firstIncompleteCheckbox = queryAllIncompleteCheckboxes(container)[0];
    fireEvent.click(firstCompleteCheckbox);
    fireEvent.click(firstIncompleteCheckbox);
    expect(queryByText(/Mark as incomplete/)).toBeInTheDocument();
    expect(queryByText(/Mark as complete/)).toBeInTheDocument();
  });

  it("should mark selected items as complete and deselect all checkboxes", () => {
    const { getByText, container } = render(<App />);
    const incompleteCheckboxes = queryAllIncompleteCheckboxes(container);
    expect(queryAllCompletedCheckboxes(container)).toHaveLength(0);
    fireEvent.click(incompleteCheckboxes[0]);
    fireEvent.click(incompleteCheckboxes[1]);
    fireEvent.click(getByText(/Mark as complete/));
    expect(queryAllCompletedCheckboxes(container)).toHaveLength(2);
    expect(
      container.querySelectorAll("input[type=checkbox]:checked")
    ).toHaveLength(0);
  });

  it("should mark selected items as incomplete and deselect all checkboxes", () => {
    const { getByText, queryAllByText, container } = render(<App />);

    // mark 2 tasks as complete
    const toggleCompleteButtons = queryAllByText(/Toggle complete/);
    fireEvent.click(toggleCompleteButtons[1]);
    fireEvent.click(toggleCompleteButtons[3]);

    // toggle 2 completed tasks as incomplete
    const completedCheckboxes = queryAllCompletedCheckboxes(container);
    expect(completedCheckboxes).toHaveLength(2);

    fireEvent.click(completedCheckboxes[0]);
    fireEvent.click(completedCheckboxes[1]);
    fireEvent.click(getByText(/Mark as incomplete/));

    // no more completed tasks
    expect(queryAllCompletedCheckboxes(container)).toHaveLength(0);

    // no more selected items
    expect(
      container.querySelectorAll("input[type=checkbox]:checked")
    ).toHaveLength(0);
  });
});

describe("show bulk actions", () => {
  const markAsComplete = true;
  const markAsIncomplete = false;

  it("given an array, returns a bool", () => {
    expect(typeof showButton(true, [])).toBe("boolean");
  });

  it('should return false for showing "mark as complete" if all selected items are complete', () => {
    const items = [
      { selected: true, complete: true },
      { selected: false, complete: true },
      { selected: true, complete: true },
    ];
    expect(showButton(markAsComplete, items)).toBe(false);
  });

  it('should return true for showing "mark as complete" if all selected items are not complete', () => {
    const items = [
      { selected: true, complete: false },
      { selected: false, complete: true },
      { selected: true, complete: false },
    ];
    expect(showButton(markAsComplete, items)).toBe(true);
  });

  it('should return true for showing "mark as complete" if mixed', () => {
    const items = [
      { selected: true, complete: true },
      { selected: false, complete: true },
      { selected: true, complete: false },
    ];
    expect(showButton(markAsComplete, items)).toBe(true);
  });

  it('should return true for showing "mark as incomplete" if all selected are complete', () => {
    const items = [
      { selected: true, complete: true },
      { selected: false, complete: true },
      { selected: true, complete: true },
    ];
    expect(showButton(markAsIncomplete, items)).toBe(true);
  });

  it('should return false for showing "mark as incomplete" if all selected are incomplete', () => {
    const items = [
      { selected: true, complete: false },
      { selected: false, complete: true },
      { selected: true, complete: false },
    ];
    expect(showButton(markAsIncomplete, items)).toBe(false);
  });
});
