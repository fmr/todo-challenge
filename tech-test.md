# Sleeping Duck tech test

This test is designed to test your fundamental React and CSS ability. There is no time limit.
If you have any questions, please email me (Matt) at matt@sleepingduck.com


## Getting started

Download [this self contained HTML](https://s3-ap-southeast-2.amazonaws.com/assets.sleepingduck.com/misc/dev-test/before.html) file. The HTML file contains all the code to run the test
and should open in any modern browser.

Your response should just be an updated version of this file. No need to create multiple files.

## Things to keep in mind:


* Make sure it stays responsive after your changes - whatever way you see fit
* You can use the latest JS/React/CSS, don't worry about the browser it's running in

## Things to do:

- [x] Move the buttons in the nav bar to always be on the right

- [x] Add another button to the nav bar with the label 'user' - it doesn't have to do anything
- Turn the list of to-dos into a **table like view** with:
  - [x] 1 column on screen sizes under `600px`
  - [x] 2 evenly spaced columns on screen sizes under` 750px`
  - [x] 3 evenly spaced columns on all other screen sizes

- Create a **bulk edit function**
  - [x]  Add a checkbox to each to-do item
  - [x]  Once at least one to-do item is selected, a floating menu at the bottom of the screen should show up
  - [x]  The menu should give the option to bulk mark as complete , or mark as incomplete
  - [x] Once one of the options are complete the appropriate action should be taken and all items should be unselected
  - [x] **Bonus points**: Only show buttons that are relevant to the selected options. For example, if only completed items are selected, only show the mark as incomplete button. Or if the there is a mix of items, show both.


## Questions

Answering these questions aren't required, but correct answers will help.

### Part 1

When setting the state within the `toggleItem` function, I used this code to modify the array:

```javascript
this.setState(({ items }) => ({
  items: items.map((item, i) =>
    index === i ? { ...item, complete: !item.complete } : item
  ),
}));
```

#### Questions


1. Why would I have passed a function to the `setState` call, rather then modifying`this.state`?
  

  **Answer:**

  >* If the state change relies on reading an existing value in `this.state`, we need to use the callback signature for `setState`
  >
  >* If we want something to fire regardless of what `shouldComponentUpdate` returns, we'd use a callback as an argument into `setState`.
  >
  >  
  >
  >**Reference:**
  >
  >https://reactjs.org/docs/react-component.html#setstate



2. What could be the reason for me mapping over the entire array rather than modifying the array directly (IE. `items[index].complete = !item[index].complete`)? And is there any possible reasons for using the object spread syntax within the loop?
  

  **Answer:**

  > * `this.state` does not get updated until the next re-render, and mutating it directly does not trigger a state change. The callback needs to return a new value to do this.
  > * spreading the item object allows us to merge that previous state with the changes we want to happen. It behaves the same way as `Object.assign(item, { complete: !item.complete })` 
  > * as a general rule, we should avoid mutations as they can have unforeseen side-effects that can lead to bugs.
  >   
  >
  > **Reference:**
  >
  > https://reactjs.org/docs/faq-state.html#why-is-setstate-giving-me-the-wrong-value
  >
  > https://css-tricks.com/understanding-react-setstate/


### Part 2

Currently, we use the array index as the way to know what to-do item we are toggling, as well as using them for the key prop when rendering the list.

#### Questions


1. Generally, what drawbacks—if any—does this have and how would you solve them?

   
   **Answer:**
   
   > If we started with an array of items keyed with the array index and subsequently reordered them, then the keys in the re-ordered items will not match the ones in the original array. If we interact with an item in the reordered array, it may not affect the original item prior to reordering.
   > 
   >The way I would solve this is to ensure that array elements are keyed by something that would uniquely identify each individual element. Ideally this would be an _identifier_ property in the object itself, like an `id`, `guid`, or a combination of properties that would be unique for each item.
   > 
   >
   >
   > **Reference:** https://reactjs.org/docs/lists-and-keys.html#keys

